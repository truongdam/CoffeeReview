<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet" type="text/css">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <!-- <link href="../../public/css/map-places.css" rel="stylesheet"> -->
    <!-- <link href="../../public/css/place-details.css" rel ="stylesheet" > -->
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<?php
  include '../../controller/show-blogs.php';

?>
<?php
include '../layout/header.php';
include '../layout/navbar.php';

 ?>
 <body>
   <div class="container">
    <div class="row">
      <div class="box">
        <section class="publicaciones-blog-home">
         <div class="container-fluid">
           <div class="">
             <h2>Ghiền  <b>Café</b></h2>
             <div class="row-page row">
               <?php
               if ($top_blog->num_rows == 1){
                 $tb = $top_blog->fetch_assoc();
                 echo '
                 <div class="col-page col-sm-8 col-md-6">
                   <a href="content.php?id='.$tb['id'].'" class="black fondo-publicacion-home">
                     <div class="img-publicacion-principal-home">
                       <img class="img-responsive" style="    object-fit: none;
                           object-position: center; " src="'.$tb["hinhanh"].'">
                     </div>
                     <div class="contenido-publicacion-principal-home">
                       <h3>'.$tb["tieude"].'</h3>
                       <p style="font-family:Arial;">'.$tb["tomtat"].'<span>...</span></p>
                     </div>
                     <div class="mascara-enlace-blog-home">
                       <span>Chi tiết </span>
                     </div>
                   </a>
                 </div>
                 ';
               }
               ?>
               <?php
               if ($blogs->num_rows > 0)
               while ($blog = $blogs->fetch_assoc()) {
                 echo '
                 <div class="col-page col-sm-4 col-md-3">
                   <a href="content.php?id='.$blog['id'].'"  class="fondo-publicacion-home">
                     <div class="img-publicacion-home">
                       <img class="img-responsive" src="'.$blog['hinhanh'].'">
                     </div>
                     <div class="contenido-publicacion-home">
                       <h3>'.$blog['tieude'].'</h3>
                       <p>'.$blog['tomtat'].'<span>...</span></p>
                     </div>
                     <div class="mascara-enlace-blog-home">
                       <span>Chi tiết </span>
                     </div>
                   </a>
                 </div>
                 ';
               }
               ?>

               <!-- <div class="col-page col-sm-4 col-md-3">
                 <a href="#" class="contenido-publicacion-home">
                     <img class="img-responsive" src="http://localhost/coffeeR/public/image/cafe.jpg">
                 </a>
               </div> -->
             </div>
           </div>
         </div>
       </section>
      </div>
    </div>
  </div>
  <?php
        include('../layout/register.php');
        include('../layout/footer.php')
   ?>
   <a href="javascript:void(0)" onclick="jQuery('html,body').animate({scrollTop: 0},1000);" class="go_top" style=""></a>
   <!-- Bootstrap Core JavaScript -->
   <script src="../../public/bootstrap/js/bootstrap.min.js"></script>
   <script src="../../public/js/banner/jssor.slider-22.2.16.mini.js" type="text/javascript"></script>
   <!-- Insert to your webpage before the </head> -->
      <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/jquery.js"></script>
      <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/amazingslider.js"></script>
      <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/initslider-1.js"></script>
      <!-- End of head section HTML codes -->
</body>
</html>
