<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['user_id'])){
  header("location:../account/login_register.php");
};
 ?>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <link href="../../public/css/map-places.css" rel="stylesheet">
    <link href="../../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script language="javascript">
        // $(document).ready(function () {
        //     $("#form-comment").submit(function (event) {
        //       event.preventDefault();
        //
        //
        //       $.ajax({
        //         url: "../../controller/place-details.php", // Url to which the request is send
        //         type: "POST",             // Type of request to be send, called as method
        //         data: {
        //           store_id:'1',
        //           comment: $("#area-comment").val()
        //         }, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        //         contentType: "false",       // The content type used when sending data to the server.
        //         cache: false,             // To unable request pages to be cached
        //         processData:false,        // To send DOMDocument or non processed data file it is set to false
        //         //dataType: "JSON",
        //         success: function(data){
        //           console.log(data);
        //         }
        //       })
        //     })
        // })

    </script>

</head>

<?php include '../layout/header.php' ?>
<?php include '../layout/navbar.php'  ?>

    <div class="container place-details">
            <div class="row">
              <!-- ----------Code PHP------------ (nam trong class row) -->

                  <?php
                  require '../../controller/connectDatabase.php';
                  $store_id = $_GET['id'];
                  $sql = " SELECT id, name, location,description, image, num_viewer, num_ranker, score_rank FROM stores WHERE id = '" .$store_id . "'";
                  $result = $conn->query($sql);
                  if( $result->num_rows === 1){
                    while ( $row = $result->fetch_assoc()) {
                      $id = $row['id'];
                      $name_store = $row['name'];
                      $location = $row["location"];
                      $link_image = $row['image'];
                      $score_rank = $row['score_rank'];
                      $num_ranker = $row['num_ranker'];
                      $description = $row['description'];
                      ?>

                      <div class="col-md-4 ">
                           <div class="thumbnail">
                             <img src="<?php echo $link_image?>" alt="">
                             <div class="caption">
                               <div class="name-place">
                                 <a href=""><?php echo $name_store ?></a>
                               </div>
                               <div class="address-place">Địa chỉ: <?php echo $location ?> </div>
                               <div class="description">Mô tả: <?php echo $description ?> </div>
                               <div class="info-place">
                                 <div class="three-col">
                                   <span class="glyphicon glyphicon-star" aria-hidden="true"></span>: <?php echo $score_rank ?>
                                 </div>
                                 <div class="three-col">
                                   <span class="glyphicon glyphicon-user " aria-hidden="true"></span>: <?php echo $num_ranker ?>
                                 </div>
                               </div>
                               <div>
                                 <a href="./rating-places.php?store=<?php echo $store_id ?> &name_store=<?php echo $name_store?>" class="btn btn-danger "> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Đánh giá</a>
                               </div>
                             </div>
                           </div>

                           <!-- <div class="panel panel-default">
                               <div class="panel-heading">
                                   <h4 class="text-center">Đánh giá<span class="glyphicon glyphicon-saved pull-right"></span></h4>
                               </div>
                               <ul class="list-group list-group-flush text-center">
                                   <li class="list-group-item">
                                       <div class="skillLineDefault">
                                           <div class="skill pull-left text-center">Không gian</div>
                                           <div class="star-rating">
                                               <div id="stars" class="starrr"></div>
                                           </div>
                                       </div>
                                   </li>
                                   <li class="list-group-item text-center">
                                       <div class="skillLineDefault">
                                           <div class="skill pull-left text-center">Giá cả</div>
                                            <div class="star-rating">
                                               <div id="stars" class="starrr"></div>
                                           </div>
                                       </div>
                                   </li>
                                   <li class="list-group-item text-center">
                                       <div class="skillLineDefault">
                                           <div class="skill pull-left text-center">Phục vụ</div>
                                            <div class="star-rating">
                                               <div id="stars" class="starrr"></div>
                                           </div>
                                       </div>
                                   </li>
                                   <li class="list-group-item text-center">
                                       <div class="skillLineDefault">
                                           <div class="skill pull-left text-center">Chất lượng</div>
                                            <div class="star-rating">
                                               <div id="stars" class="starrr"></div>
                                           </div>
                                       </div>
                                   </li>
                               </ul>
                               <div class="panel-footer text-center">
                                   <button type="button" class="btn btn-primary btn-lg btn-block">
                                       Rate
                                   </button>
                               </div>
                           </div> -->

                       </div>






            <div class="col-md-8 well">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget-area no-padding blank">
                                <div class="status-upload">
                                    <form action="../../controller/place-details.php" method="POST" name="form-comment" id="form-comment">
                                        <textarea id="area-comment" name="area-comment" placeholder="Hãy để lại bình luận của bạn..."  required></textarea>
                                        <input name="store_id" type="hidden" value="<?php echo $store_id ?>"/>
                                        <ul>
                                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Smile"><i class="fa fa-smile-o"></i></a></li>
                                            <li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
                                        </ul>
                                        <button type="submit" class="btn btn-success green"><i class="fa fa-share"></i> Bình luận</button>
                                    </form>
                                </div><!-- Status Upload  -->
                            </div><!-- Widget Area -->

                        </div>
                    </div>
            <?php
        }
        }
        ?>

        <?php
          $sql = " SELECT id, name, location,description, image, num_viewer, num_ranker, score_rank FROM stores WHERE id = '" .$store_id . "'";
          $result = $conn->query($sql);

        ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="comments-container">
                                <ul id="comments-list" class="comments-list">

                                    <?php

                                    require '../../controller/connectDatabase.php';
                                    $store_id = $_GET['id'];
                                    $sql = " SELECT id, comment, user_id, time_comment FROM discussion WHERE store_id = '" .$store_id . "' ORDER BY time_comment DESC";
                                    // var_dump ($sql);
                                    $result = $conn->query($sql);
                                    if( $result->num_rows >0){
                                      while ( $row = $result->fetch_assoc()) {
                                        $id = $row['id'];
                                        $comment = $row['comment'];
                                        $user_id= $row["user_id"];
                                        $time_comment = $row['time_comment'];

                                        $sql2 = "SELECT id,name,image FROM users WHERE id='".$user_id ."'";
                                        $user =$conn->query($sql2)->fetch_assoc();
                                        $user_image = $user['image'];
                                        $user_name =$user['name'];
                                        ?>
                                        <li>
                                            <div class="comment-main-level">
                                                <!-- Avatar -->
                                                <div class="comment-avatar">
                                                    <img src="<?php echo $user_image ?>" alt="">
                                                </div>
                                                <!-- Comment box -->
                                                <div class="comment-box">
                                                    <div class="comment-head">
                                                        <h6 class="comment-name">
                                                            <a href=""><?php echo $user_name ?></a>
                                                        </h6>
                                                        <span>bình luận <?php echo $time_comment ?></span>
                                                        <i class="fa fa-reply"></i>
                                                    </div>

                                                    <div class="comment-content">
                                                        <?php echo $comment ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>


                                        <?php
                                    }
                                    }




                                     ?>

                                </ul>

                            </div>
                        </div>
                    </div>
            </div>
        </div>



<!-------------- End Code PHP------- (nam trong class row) -->

    <script src="../../public/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        // Starrr plugin (https://github.com/dobtco/starrr)
            var __slice = [].slice;

            (function($, window) {
              var Starrr;

              Starrr = (function() {
                Starrr.prototype.defaults = {
                  rating: void 0,
                  numStars: 5,
                  change: function(e, value) {}
                };

                function Starrr($el, options) {
                  var i, _, _ref,
                    _this = this;

                  this.options = $.extend({}, this.defaults, options);
                  this.$el = $el;
                  _ref = this.defaults;
                  for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                      this.options[i] = this.$el.data(i);
                    }
                  }
                  this.createStars();
                  this.syncRating();
                  this.$el.on('mouseover.starrr', 'span', function(e) {
                    return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('mouseout.starrr', function() {
                    return _this.syncRating();
                  });
                  this.$el.on('click.starrr', 'span', function(e) {
                    return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('starrr:change', this.options.change);
                }

                Starrr.prototype.createStars = function() {
                  var _i, _ref, _results;

                  _results = [];
                  for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
                  }
                  return _results;
                };

                Starrr.prototype.setRating = function(rating) {
                  if (this.options.rating === rating) {
                    rating = void 0;
                  }
                  this.options.rating = rating;
                  this.syncRating();
                  return this.$el.trigger('starrr:change', rating);
                };

                Starrr.prototype.syncRating = function(rating) {
                  var i, _i, _j, _ref;

                  rating || (rating = this.options.rating);
                  if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                    }
                  }
                  if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                    }
                  }
                  if (!rating) {
                    return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                  }
                };

                return Starrr;

              })();
              return $.fn.extend({
                starrr: function() {
                  var args, option;

                  option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                  return this.each(function() {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                      $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                      return data[option].apply(data, args);
                    }
                  });
                }
              });
            })(window.jQuery, window);

            $(function() {
              return $(".starrr").starrr();
            });

            $( document ).ready(function() {

              $('#stars').on('starrr:change', function(e, value){
                $('#count').html(value);
              });

              $('#stars-existing').on('starrr:change', function(e, value){
                $('#count-existing').html(value);
              });
            });

    </script>
    </div>
    <!-- /.container -->
<?php include '../layout/footer.php' ?>
