<script language="javascript">


  $(document).ready(function(){

    $("#form-search").submit(function (event) {

      event.preventDefault();
      var search =$("#search").val();
      console.log(search);
      $.ajax({
        url: "../controller/search-places.php", // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,
        dataType:"html",      // To send DOMDocument or non processed data file it is set to false
        success: function(data){
          console.log(data);
          $("#liststore_content").html( data);
        }
      })


    })
  });
</script>
<div class="content">
      <div class="row search-bar">
          <div class="col-md-12">
            <form  id="form-search">
              <div class="input-group">
                      <div class="input-group-btn search-panel">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span id="search_concept">Lọc theo quận</span> <span class="caret"></span>
                          </button>

                          <table class="dropdown-menu table_district" >
                            <tr>
                              <td><input type="checkbox" name="address1" value="quận 1"> Quận 1 </td>
                              <td> <input type="checkbox" name="address2" value="quận 4"> Quận 4</td>
                              <td> <input type="checkbox" name="address3" value="quận 7"> Quận 7 </td>
                              <td> <input type="checkbox" name="address4" value="quận 10"> Quận 10 </td>
                              <td> <input type="checkbox" name="address5" value="quận tân bình"> Quận Tân Bình</td>

                            </tr>
                            <tr>
                              <td> <input type="checkbox" name="address6" value="quận 2">Quận 2 </td>
                              <td> <input type="checkbox" name="address7" value="quận 5"> Quận 5</td>
                              <td> <input type="checkbox" name="address8" value="quận 8"> Quận 8</td>
                              <td> <input type="checkbox" name="address9" value="quận 11"> Quận 11</td>
                              <td> <input type="checkbox" name="address10" value="quận bình thạnh"> Quận Bình Thạnh</td>
                            </tr>
                            <tr>
                              <td> <input type="checkbox" name="address11" value="quận 3"> Quận 3</td>
                              <td> <input type="checkbox" name="address12" value="quận 6"> Quận 6 </td>
                              <td> <input type="checkbox" name="address13" value="quận 9"> Quận 9 </td>
                              <td> <input type="checkbox" name="address14" value="quận 12"> Quận 12</td>
                              <td> <input type="checkbox" name="address15" value="quận thủ đức"> Quận Thủ Đức</td>

                            </tr>

                          </table>
                      </div>
                      <!-- Split button -->

                      <input type="text" class="form-control" name="search" id="search" placeholder="Tên quán cafe, địa chỉ ...">
                      <span class="input-group-btn">
                          <button type="submit" class="btn btn-default" type="button" ><span class="glyphicon glyphicon-search"></span></button>

                          <a href="./place/create-places.php" class="btn btn-default" role="button"><span class="glyphicon glyphicon-plus"></span></a>
                      </span>
              </div>
            </form>
          </div>


      </div>





      <div class="row">
          <div class="box" id="liststore_content">


            <!-- ------------------Code PHP------------------- -->

            <?php
              require '../controller/connectDatabase.php';
              $sql = " SELECT id, name, location, image, num_viewer, num_ranker, score_rank FROM stores ORDER BY score_rank DESC ";
              $result = $conn->query($sql);
              if( $result->num_rows){
                while ( $row = $result->fetch_assoc()) {
                  $id = $row['id'];
                  $name_store = $row['name'];
                  $location = $row["location"];
                  $link_image = $row['image'];
                  $score_rank = $row['score_rank'];
                  $num_ranker = $row['num_ranker'];
                  $brief_location ="";
                  if(strlen($location)>65){
                    $brief_location = substr($location,0,strpos($location,'TP')) . "..." ;
                  }
                  else {
                    $brief_location =$location;
                  }

                  # code...
              ?>
                  <div class="col-lg-4" >
                    <div class="thumbnail" id="list-store">
                        <img src=" <?php echo $link_image ; ?> " alt="<?php echo $name_store ?>">
                        <div class="caption">
                          <div class="name-place">
                            <a href="./place/map-places.php?store_id=<?php echo $id ?>"> <?php echo $name_store; ?> </a>
                          </div>
                          <div class="address-place"> <?php echo $brief_location; ?></div>
                          <div class="info-place">
                            <div class="three-col">
                              <span class="glyphicon glyphicon-star" aria-hidden="true"></span>: <?php echo $score_rank; ?>
                            </div>
                            <div class="three-col">
                              <span class="glyphicon glyphicon-user " aria-hidden="true"></span>: <?php echo $num_ranker; ?>
                            </div>
                            <div class="three-col align-right">
                              <a href="./place/place-details.php?id= <?php echo $id ?>"  class="btn btn-default btn-sm" role="button">Chi tiết</a>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>

              <?php
                }
              }

             ?>



              <!-- ------------------------End Code PHP ------------------------- -->
          </div>
      </div>

</div>
<div class="col-page col-sm-4 col-md-3">
  <a href="#" class="contenido-publicacion-home">
      <img class="img-responsive" src="http://localhost/coffeeR/public/image/back_top.jpg">
  </a>
</div>


<script>
$(document).ready(function(){
    $(".dropdown-toggle").dropdown();
});
</script>
