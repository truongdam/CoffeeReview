<?php
if(!isset($_SESSION)){
    @session_start();
}

 ?>
<!-- Navigation -->
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
            <a class="navbar-brand" href="index.html">R Coffee</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav menu">
                <li>
                    <a href="http://localhost/coffeeR/views">HOME</a>
                </li>
                <li>
                    <a href="http://localhost/coffeeR/views/place/map-places.php">PLACES</a>
                </li>
                <li>
                    <a href="http://localhost/coffeeR/views/blog/blog.php">BLOG</a>
                </li>



            <!-------------------------CODE LOGIN POPUP------------------- -->

            <!--
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                <ul id="login-dp" class="dropdown-menu">
                  <li>
                     <div class="row">
                        <div class="col-md-12">
                          Login via
                          <div class="social-buttons">
                            <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                            <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                          </div>
                                          or
                           <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                              <div class="form-group">
                                 <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                 <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                              </div>
                              <div class="form-group">
                                 <label class="sr-only" for="exampleInputPassword2">Password</label>
                                 <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                       <div class="help-block text-right"><a href=""style="color:black;">Forget the password ?</a></div>
                              </div>
                              <div class="form-group">
                                 <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                              </div>
                              <div class="checkbox">
                                 <label>
                                 <input type="checkbox"> Keep me logged-in
                                 </label>
                              </div>
                           </form>
                        </div>
                        <div id="join-us" class="bottom text-center">
                          New here ? <a href="#" style="color:black;" data-toggle="modal" data-target="#registerModal"><b>Join Us</b></a>
                        </div>
                     </div>
                  </li>
                </ul>
              </li> -->


              <!-- --------------------END CODE LOGIN POPUP----------------------- -->

            </ul>
    <ul class="nav navbar-nav navbar-right menu">

      <?php
          if(isset($_SESSION['username'])){
          ?>
              <li class="dropdown">
                <a href="" class="dropdown-toggle" data-toggle="dropdown" >
                  <span class="glyphicon glyphicon-user"></span>
                  <strong><?php echo $_SESSION['username'] ?></strong>
                  <span class="glyphicon glyphicon-chevron-down"></span>
                </a>
                <ul class="dropdown-menu">
                    <li class="divider navbar-login-session-bg"></li>
                     <li><a href="http://localhost/coffeeR/views/account/account.php">Account  <span class="glyphicon glyphicon-cog pull-right"></span></a></li>

                     <?php if(isset($_SESSION['admin'])){
                       ?>
                       <li class="divider"></li>
                       <li><a href="http://localhost/coffeeR/views/blog/create-blog.php">Create Blog </a></li>
                     <?php
                     } ?>


                    <li class="divider"></li>
                    <li><a href="http://localhost/coffeeR/controller/logout.php">Sign out <span class="glyphicon glyphicon-log-out pull-right"></span></a></li>

                  </ul>
              </li>
      <?php

          }
          else {
      ?>
      <li>
          <a href="http://localhost/coffeeR/views/account/login_register.php">LOGIN</a>
      </li>
      <?php
          }

       ?>

    </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
