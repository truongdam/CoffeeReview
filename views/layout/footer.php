<footer>
    <div class="container">
        <div class="row foot">
          <div class="col-md-5">
            <div class="clear"></div>
            <div class="copyright">R Coffe &copy; BK Team </div>
          </div>
          <div class="col-md-7">

            <div class="social float-right">
              <div class="row">
                <div class="col-md-4">



                  <a href="#" class="fb">
                      <span data-icon="f" class="icon"></span>
                      <span class="info">
                          <span class="follow">Become a fan Facebook</span>
                          <span class="num">9,99</span>
                      </span>
                  </a>
                  </div>
                    <div class="col-md-4">
                  <a href="#" class="tw">
                      <span data-icon="T" class="icon"></span>
                      <span class="info">
                          <span class="follow">Follow us Twitter</span>
                          <span class="num">9,999</span>
                      </span>
                  </a>
                </div>
                    <div class="col-md-4">
                  <a href="#" class="rss">
                      <span data-icon="R" class="icon"></span>
                      <span class="info">
                          <span class="follow">Subscribe RSS</span>
                          <span class="num">9,999</span>
                      </span>
                  </a>
                  </div
                </div>
            </div>
          </div>
        </div>
    </div>
</footer>
